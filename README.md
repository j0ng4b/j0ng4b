<div align="center">
  <h1>Hello! 👋</h1>
</div>
<p>
  I'm Jonatha Gabriel, j0ng4b for short, a self-taught programmer that's likes low level programming or almost low level 🙂.
  <br>
  I love chess, animes, mangas, games and programming as well and open source software.
  <br>
  Oh, also, I know how to close Vim :q
</p>
<br>
<div align="center">
  <h2>Languages and Tools</h2>
  <img src="https://img.shields.io/badge/C%20Language-222222?style=flat-square&logo=c&logoColor=white&labelColor=00599C">
  <img src="https://img.shields.io/badge/Shell%20Script-222222?style=flat-square&logo=shell&logoColor=white&labelColor=FF9900">
  <br>
  <img src="https://img.shields.io/badge/Vim-222222?style=flat-square&logo=vim&logoColor=black&labelColor=11AB00">
  <img src="https://img.shields.io/badge/Git-222222?style=flat-square&logo=git&logoColor=black&labelColor=F05033">
</div>
<br>
<div align="center">
  <h2>Reach me on</h2>
  <a href="https://discord.com/users/676795309627670543">
    <img src="https://img.shields.io/badge/Discord-5865F2?style=for-the-badge&logo=discord&logoColor=white">
  </a>
  <a href="https://www.reddit.com/u/j0ng4b">
    <img src="https://img.shields.io/badge/Reddit-FF4500?style=for-the-badge&logo=reddit&logoColor=white">
  </a>
  <a href="https://twitter.com/j0ng4b">
    <img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white">
  </a>
  <br>
  <a href="https://www.instagram.com/j0ng4b">
    <img src="https://img.shields.io/badge/Instagram-E1306C?style=for-the-badge&logo=instagram&logoColor=white">
  </a>
  <a href="https://stackoverflow.com/users/17138393/j0ng4b">
    <img src="https://img.shields.io/badge/Stack_Overflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white">
  </a>
  <a href="https://anilist.co/user/j0ng4b">
    <img src="https://img.shields.io/badge/AniList-1B2230?style=for-the-badge&logo=anilist&logoColor=white">
  </a>
</div>
